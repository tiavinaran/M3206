Exercice 1 :
vim exo_01.txt	# Lancement de l'édition du fichier
6j		# Aller 6 lignes au dessous
i		# Insérer du texte
*temp var7 11
<entrée>	# Aller a la ligne
<esc>		# Quitter le mode ins�rtion
3j		# Descendre encore de 3 lignes
o		# Ouvrir une nouvelle ligne et ins�rer le texte
New text.
<entrée>	# Allera la ligne
<esc>		# Passe en mode édition
:wq		# Enregistrement

Exercice 2 :
vim exo_02.txt	# On ouvre le ficher
:%s/Sed/XXX/g	# On remplace toute les Sed par XXX dans ce fichier gr�ce a %
:%s/sed/xxx/g

Exercice 3 :
vim exo_03.txt
:g/def/d
<touche_directionnelle_haut>
<tab>
<texte>
<esc>
:wq

Exercice 4 :
vim exo_04.txt
99riii		# On remplace les 99 caract�res en i puis on ins�re i
