#!bin/sh
tools="git tmux vim htop"

for i in $tools; do
	dpkg -l | grep $i > /dev/null
	if [ $? -eq 0 ]; then
		echo "[...] $i: install� [...]"
	else
		echo "[/!\\] $i: pas installé [/!\\] lancer la commande:\apt-get install $i\`"
	fi
done
