#!/bin/bash
re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then # On teste si l'argument ajoutépar l'utilisateur n'est pas un nombre
	echo "error: $1 not a number" >&2
	exit 1
else # Si l'argument est bien un nombre
	awk 'BEGIN{FS=";"}''{print $2}' my_history | awk '{print $1}' | sort | uniq -c | sort -rn | head -n $1
	# Il suffit juste d'afficher l'history mais en appliquant des "filtres"
	# On modifie la m�thode de s�paration par le ";" 
	# On affiche l'history, plus precisement qui est le premier mot de chaque ligne du fichier
	# On refait un awk pour prendre que le premier mot de chaque ligne de commande
	# On le tri selon les commandes
	# On rend les commandes uniques (sans r�t�tition) et en assignant le nombre de r�p�titions de la commandes dans history
	# Apr�s on tri selon le nombre de r�p�titions
	# On prends que les $1 premi�res lignes  
	exit 0
fi	
