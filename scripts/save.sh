#!/bin/bash
[[ -d $1 ]]
if [[ $? -eq 0 ]] ; then # On teste si l'argument ajouté par l'utilisateur est bien un répertoire
	nom=$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz # Le nom du fichier
	tar cvzf $nom $1 # On compresse le r�pertoire
	mv $nom /tmp/backup/. 
	echo "creation de l'archive: $nom"
	exit 0
else # Si l'argument est bien un nombre
	echo "error : $1 is not a directory"
	exit 1
fi	
