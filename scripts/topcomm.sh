#/bin/sh!
re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then # On teste si l'argument ajoutépar l'utilisateur n'est pas un nombre
	echo "error: $1 not a number" >&2
	exit 1
else # Si l'argument est bien un nombre
	awk '{print $1}' ~/.bash_history | sort | uniq -c | sort -rn | head -n $1
	# Il suffit juste d'afficher l'history mais en appliquant des "filtres" 
	# On affiche l'history, plus pr�cisement $1 qui est le premier mot de chaque ligne du fichier
	# On le tri selon les commandes
	# On rend les commandes uniques (sans r�t�tition) et en assignant le nombre de r�p�titions de la commandes dans history
	# Apr�s on tri selon le nombre de r�p�titions
	# On prends que les $1 premi�res lignes  
	exit 0
fi	
