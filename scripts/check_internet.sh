#!/bin/sh
wget -q --spider http://google.com
# Le -q signifie "quiet", c'est le mode silence
# Le --spider permet de ne pas t�l�charger la page mais juste tester sa pr�sence
echo "[...] checking internet connection [...]"
if [ $? -eq 0 ]; then # Le $? contient la r�ponse de la requ�te pr�c�dente
	echo "[...] Internet access OK [...]"
	exit 0 # Tout s'est bien passé 
else
	echo "[/!\\] No connected to Internet [/!\\]\n[/!\\] Please check configuration [/!\\]"
	exit 1
fi
